
let defaultProps = {flavor: "vanilla", sprinkles: "lots"}

let defaults = function (obj , defaultProps){
    let arr = []
    if (!obj){
        obj = defaultProps
    }
    for (let key in obj){
        arr.push([key,obj[key]])

    }
    return arr
}

module.exports = defaults;