let mapObject = function(obj,cb){

    let array = []
    for (let key in obj){
        array.push([key,cb(obj[key])])
    }
    return array


}

module.exports = mapObject;